
def tofloatifpossible(s):
	try:
		return float(s)
	except ValueError:
		return s

categoryNames = input("Enter the four categories, separated by commas\n\t").split(", ")
categories = []
for i in range(4):
	text = "Enter the four possible values for " + categoryNames[i] + "\n\t"
	values = input(text).split(", ")
	values = [tofloatifpossible(val) for val in values]
	categories.append(values)

categoryIndex = {categoryNames[i]: i for i in range(4)}

#index of the category in which the input can be cound
def catindex(name):
	for i in range(len(categories)):
		if name in categories[i]:
			return i
	return None

#index of input within its category 
def index(name):
	for category in categories:
		if name in category:
			return category.index(name)

def remaining(a, b, c):
	for i in range(4):
		if a != str(i) and b != str(i) and c != str(i):
			return str(i)

def decode(code):
	decoded = "0" + code[:3] + "1" + code[3:6] + "2" + code[6:9] + "3" + code[9:]
	decoded += remaining(code[0],code[3],code[6])
	decoded += remaining(code[1],code[4],code[7])
	decoded += remaining(code[2],code[5],code[8])
	organized = [
		[int(i) for i in decoded[:4]],
		[int(i) for i in decoded[4:8]],
		[int(i) for i in decoded[8:12]],
		[int(i) for i in decoded[12:16]]
		]
	return organized

codes = []
for i1 in range(4):
	code = str(i1)
	for i2 in range(4):
		code += str(i2)
		for i3 in range(4):
			code += str(i3)
			for j1 in [i for i in range(4) if i != i1]:
				code += str(j1)
				for j2 in [i for i in range(4) if i!= i2]:
					code += str(j2)
					for j3 in [i for i in range(4) if i!= i3]:
						code += str(j3)
						for k1 in [i for i in range(4) if i!= i1 and i!= j1]:
							code += str(k1)
							for k2 in [i for i in range(4) if i!= i2 and i!= j2]:
								code += str(k2)
								for k3 in [i for i in range(4) if i!= i3 and i!= j3]:
									code += str(k3)
									codes.append(code)
									code = code[:-1]
								code = code[:-1]
							code = code[:-1]
						code = code[:-1]
					code = code[:-1]
				code = code[:-1]
			code = code[:-1]
		code = code[:-1]
	code = code[:-1]
worlds = [decode(code) for code in codes]

#CLUES

def AisB(name1, name2, world):
	i1,j1 = catindex(name1), index(name1)
	i2,j2 = catindex(name2), index(name2)
	#loop through each individual in world
	for i in range(len(world)):
		#check if that individual is name1
		if world[i][i1] == j1:
			#check if they're also name2
			return True if world[i][i2] == j2 else False

def AisntB(name1, name2, world):
	return not AisB(name1, name2, world)

def AisBorC(name1,name2,name3,world):
	return AisB(name1,name2,world) ^ AisB(name1,name3,world)

def AisneitherBnorC(name1,name2,name3,world):
	isnt12 = AisntB(name1,name2,world)
	isnt13 = AisntB(name1,name3,world)
	isnt23 = AisntB(name2,name3,world)
	return isnt12 and isnt13 and isnt23

def ABCaredifferent(name1,name2,name3,world):
	return AisneitherBnorC(name1,name2,name3,world)

# Of A and B, one is C and the other is D
def ofABoneCotherD(name1,name2,name3,name4,world):
	presupposition = AisntB(name1,name2,world) and AisntB(name3,name4,world)
	possibility1 = AisB(name1,name3,world) and AisB(name2,name4,world)
	possibility2 = AisB(name1,name4,world) and AisB(name2,name3,world)
	return presupposition and (possibility1 or possibility2)

# A is less than B in the specified category
def AlessthanB(name1,name2,category,world):
	cat = categoryNames.index(category)
	i1,j1 = catindex(name1), index(name1)
	i2,j2 = catindex(name2), index(name2)
	#loop through individuals
	for i in range(len(world)):
		for j in range(len(world)):
			#find the two individuals
			if world[i][i1] == j1 and world[j][i2] == j2:
				index1 = world[i][cat]
				index2 = world[j][cat]
				if categories[cat][index1] < categories[cat][index2]:
					return True
				else:
					return False

def AmorethanB(name1,name2,category,world):
	return AlessthanB(name2,name1,category,world)

def AlessthanBbyX(name1,name2,category,x,world):
	cat = categoryNames.index(category)
	i1,j1 = catindex(name1), index(name1)
	i2,j2 = catindex(name2), index(name2)
	#loop through individuals
	for i in range(len(world)):
		for j in range(len(world)):
			#find the two individuals
			if world[i][i1] == j1 and world[j][i2] == j2:
				index1 = world[i][cat]
				index2 = world[j][cat]
				if categories[cat][index1] + x == categories[cat][index2]:
					return True
				else:
					return False

def AmorethanBbyX(name1,name2,category,x,world):
	return AlessthanBbyX(name2,name1,category,x,world)

def prettyPrint(world):
	for person in world:
		description = ""
		for i in range(3):
			description += str(categories[i][person[i]]) + " is "
		description += str(categories[3][person[3]])
		print(description)

print()
print("Types of clues:\n\t" + 
	"1. A is B\n\t" + 
	"2. A isn't B\n\t" +  
	"3. A is either B or C\n\t" +  
	"4. A is neither B nor C\n\t" +  
	"5. A, B, and C are all different\n\t" +  
	"6. Of A and B, one is C and the other is D\n\t" + 
	"7. A is less than B in category X\n\t" + 
	"8. A is more than B in category X\n\t" + 
	"9. A is less than B in category X by amount N\n\t" + 
	"10. A is more than B in category X by amount N\n")
func = [AisB,AisntB,AisBorC,AisneitherBnorC,ABCaredifferent,ofABoneCotherD,
	AlessthanB,AmorethanB,AlessthanBbyX,AmorethanBbyX]
clues = []
running = True
while running:
	clueType = input("Enter clue type (0 if no more clues): ")
	parameters = ""
	if clueType == "0":
		running = False
		break
	elif clueType == "1":
		parameters = input("\tA is B. Specify A and B:\n\t\t").split(", ")
	elif clueType == "2":
		parameters = input("\tA isn't B. Specify A and B:\n\t\t").split(", ")
	elif clueType == "3":
		parameters = input("\tA is either B or C. Specify A, B, and C:\n\t\t").split(", ")
	elif clueType == "4":
		parameters = input("\tA is neither B nor C. Specify A, B, and C:\n\t\t").split(", ")
	elif clueType == "5":
		parameters = input("\tA, B, and C are all different. Specify A, B, and C:\n\t\t").split(", ")
	elif clueType == "6":
		parameters = input("\tOf A and B, one is C and the other is D. Specify A, B, C, and D:\n\t\t").split(", ")
	elif clueType == "7":
		parameters = input("\tA is less than B in category X. Specify A, B, and X:\n\t\t").split(", ")
	elif clueType == "8":
		parameters = input("\tA is more than B in category X. Specify A, B, and X:\n\t\t").split(", ")
	elif clueType == "9":
		parameters = input("\tA is less than B in category X by amount N. Specify A, B, X, and N:\n\t\t").split(", ")
	elif clueType == "10":
		parameters = input("\tA is more than B in category X by amount N. Specify A, B, X, and N:\n\t\t").split(", ")
	else:
		print("\tI think you messed up")
		break
	parameters = [tofloatifpossible(p) for p in parameters]
	try:
		clue = [func[int(clueType)-1](*parameters, world) for world in worlds]
		clues.append(clue)
	except TypeError:
		print("\tYour inputs didn't make sense. Try again.")

print()
for i in range(len(worlds)):
	numSatisfied = 0
	for clue in clues:
		numSatisfied += clue[i]
	if numSatisfied == len(clues):
		prettyPrint(worlds[i])
		print()


