
# Logic Puzzle Solver

This is a python program that automatically solves any 4x4 logic grid puzzle (four individuals and four categories). Works well with the 4x4 puzzles on [Puzzle Baron](https://logic.puzzlebaron.com/).

## How it works

First, enter in the names of the four categories. 

Then enter in the four possible values for each category. 

Finally, enter in the clues, one at a time. This is done by specifying the type of clue (e.g. "A is either B or C"), and then the specific names in the clue (e.g. "Dave is either 24 or a doctor" would be input as "Dave, 24, doctor").

And enter in your solution!

## Sample solve

Here's the program solving the most difficult category of puzzle on Puzzle Baron.

![](Media/sample-solve.m4v)

![](Media/sample-solve.png)